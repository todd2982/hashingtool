﻿'HashingTool used to easily create MD5SUM for a string of text or file.
'Copyright (C) 2012  Todd Stark
'This program is free software: you can redistribute it and/or modify
'it under the terms of the GNU General Public License as published by
'the Free Software Foundation, either version 3 of the License, or
'(at your option) any later version.
'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.
'You should have received a copy of the GNU General Public License
'along with this program.  If not, see <http://www.gnu.org/licenses/>.

<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmMain1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.cmdMD5StringCalc = New System.Windows.Forms.Button()
        Me.txtStringInput = New System.Windows.Forms.TextBox()
        Me.lblInput = New System.Windows.Forms.Label()
        Me.lblOutput = New System.Windows.Forms.Label()
        Me.txtOutput = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'cmdMD5StringCalc
        '
        Me.cmdMD5StringCalc.Location = New System.Drawing.Point(197, 90)
        Me.cmdMD5StringCalc.Name = "cmdMD5StringCalc"
        Me.cmdMD5StringCalc.Size = New System.Drawing.Size(75, 23)
        Me.cmdMD5StringCalc.TabIndex = 0
        Me.cmdMD5StringCalc.Text = "MD5 String"
        Me.cmdMD5StringCalc.UseVisualStyleBackColor = True
        '
        'txtStringInput
        '
        Me.txtStringInput.Location = New System.Drawing.Point(12, 25)
        Me.txtStringInput.Name = "txtStringInput"
        Me.txtStringInput.Size = New System.Drawing.Size(260, 20)
        Me.txtStringInput.TabIndex = 1
        '
        'lblInput
        '
        Me.lblInput.AutoSize = True
        Me.lblInput.Location = New System.Drawing.Point(12, 9)
        Me.lblInput.Name = "lblInput"
        Me.lblInput.Size = New System.Drawing.Size(82, 13)
        Me.lblInput.TabIndex = 2
        Me.lblInput.Text = "Text to convert:"
        '
        'lblOutput
        '
        Me.lblOutput.AutoSize = True
        Me.lblOutput.Location = New System.Drawing.Point(12, 48)
        Me.lblOutput.Name = "lblOutput"
        Me.lblOutput.Size = New System.Drawing.Size(57, 13)
        Me.lblOutput.TabIndex = 3
        Me.lblOutput.Text = "MD5SUM:"
        '
        'txtOutput
        '
        Me.txtOutput.Location = New System.Drawing.Point(15, 64)
        Me.txtOutput.Name = "txtOutput"
        Me.txtOutput.ReadOnly = True
        Me.txtOutput.Size = New System.Drawing.Size(257, 20)
        Me.txtOutput.TabIndex = 4
        '
        'frmMain1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(284, 122)
        Me.Controls.Add(Me.txtOutput)
        Me.Controls.Add(Me.lblOutput)
        Me.Controls.Add(Me.lblInput)
        Me.Controls.Add(Me.txtStringInput)
        Me.Controls.Add(Me.cmdMD5StringCalc)
        Me.Name = "frmMain1"
        Me.Text = "Hashing Tool"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cmdMD5StringCalc As System.Windows.Forms.Button
    Friend WithEvents txtStringInput As System.Windows.Forms.TextBox
    Friend WithEvents lblInput As System.Windows.Forms.Label
    Friend WithEvents lblOutput As System.Windows.Forms.Label
    Friend WithEvents txtOutput As System.Windows.Forms.TextBox

End Class
