﻿'HashingTool used to easily create MD5SUM for a string of text or file.
'Copyright (C) 2013  Todd Stark
'This program is free software: you can redistribute it and/or modify
'it under the terms of the GNU General Public License as published by
'the Free Software Foundation, either version 3 of the License, or
'(at your option) any later version.
'This program is distributed in the hope that it will be useful,
'but WITHOUT ANY WARRANTY; without even the implied warranty of
'MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
'GNU General Public License for more details.
'You should have received a copy of the GNU General Public License
'along with this program.  If not, see <http://www.gnu.org/licenses/>.

Imports System.Security.Cryptography
Imports System.Text

Public Class frmMain1

    Private Sub cmdMD5StringCalc_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cmdMD5StringCalc.Click
        Dim md5calc As New MD5CryptoServiceProvider
        Dim md5Byte() As Byte
        Dim md5sum As String
        Dim textIN As String
        Dim textData() As Byte
        textIN = txtStringInput.Text
        textData = Encoding.UTF8.GetBytes(textIN)
        md5Byte = md5calc.ComputeHash(textData)
        md5sum = ByteArrayToString(md5Byte)
        txtOutput.Text = md5sum

    End Sub

    Private Function ByteArrayToString(ByVal arrInput() As Byte) As String

        Dim strOutput As New System.Text.StringBuilder(arrInput.Length)

        For i As Integer = 0 To arrInput.Length - 1
            strOutput.Append(arrInput(i).ToString("X2"))
        Next

        Return strOutput.ToString().ToLower

    End Function

    Private Sub frmMain1_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

    End Sub
End Class
